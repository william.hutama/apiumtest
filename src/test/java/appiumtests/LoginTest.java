package appiumtests;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
public class LoginTest {

	
	static AppiumDriver<MobileElement> driver;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			openLogin();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("TAM");
			System.out.println(e.getCause());
			System.out.println("----------------------------------");
			System.out.println(e.getMessage());
			System.out.println("----------------------------------");
			e.printStackTrace();
		}
	}
	
	@Test
	public static void openLogin() throws Exception {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability("deviceName", "OnePlus 5");
		cap.setCapability("udid", "127.0.0.1:52337");
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "9");
		
//		cap.setCapability("deviceName", "SM-A507FN/DS");
//		cap.setCapability("udid", "RR8M905WALY");
//		cap.setCapability("platformName", "Android");
//		cap.setCapability("platformVersion", "11");
		
		cap.setCapability("appPackage", "com.generali.iclick.paymentdev");
		cap.setCapability("appActivity", "com.generali.iclick.paymentdev.MainActivity");
		
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url, cap);
		
		System.out.println("Application Started !!! Toyota");
		
		driver.manage().timeouts().implicitlyWait(20000, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath("//android.widget.EditText[@password='false']")).sendKeys("paula_kvn@hotmail.com");
		driver.findElement(By.xpath("//android.widget.EditText[@password='true']")).sendKeys("Generali-123");
		
		driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]")).click();
		//driver.findElement(By.xpath("//android.view.ViewGroup[@instance='15']")).click();
		//driver.findElement(By.xpath("//android.view.ViewGroup[@instance='15']")).click();
		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]")).click();
		

		System.out.println("DONE !!!");
		
		
	}

}
